#Feature: Verifying 7Targets Functionalities
#
#Background:
    #Given I am on testdrive login page
    #
 #Scenario: Add lead and check status
 #Given I enter username as "ggnagarkar+300@gmail.com" and password as "abcd123"
 #When I click on login button
 #Then I should be on "Dashboard" page
 #When I navigate to Leads page
 #And I add "normal" Leads to Leads page
 #Then I validate lead created
 #Then I validate "Cold" status on created lead
 #And I wait for "10" min to change status
 #Then I validate "Processed" status on created lead
 #
 #
 #Scenario: Check deactivated status of lead
 #Given I enter username as "ggnagarkar+300@gmail.com" and password as "abcd123"
 #When I click on login button
 #When I navigate to Leads page
 #And I add "deactivated" Leads to Leads page
 #Then I validate lead created
 #Then I validate "Deactivated" status on created lead
 #
 #Scenario: Check Unsubscribed status of lead
 #Given I enter username as "ggnagarkar+300@gmail.com" and password as "abcd123"
 #When I click on login button
 #When I navigate to Leads page
 #And I add "Unsubscribed" Leads to Leads page
 #And I validate lead created
 #And I click on created lead
 #And I click on "Unsubscribe" button on lead page
 #Then I validate "Unsubscribed" status on created lead
 #And I add "Unsubscribed" Leads to Leads page
 #Then I validate add lead page display error message
  #
  #
 #Scenario: Check message of created lead
 #Given I enter username as "ggnagarkar+300@gmail.com" and password as "abcd123"
 #When I click on login button
 #Then I should be on "Dashboard" page
 #When I navigate to Leads page
 #And I add "normal" Leads to Leads page
 #And I click on created lead
 #Then I validate message lead page
 #
 #
 #Scenario: Check for all messages of created lead
 #Given I enter username as "ggnagarkar+300@gmail.com" and password as "abcd123"
 #When I click on login button
 #Then I should be on "Dashboard" page
 #When I navigate to Leads page
 #And I add "normal" Leads to Leads page
 #And I click on created lead
 #And I click on "Message" button on lead page
 #Then I validate all message lead page
 #
 #Scenario: Check Pagination of leads page
 #Given I enter username as "ggnagarkar+300@gmail.com" and password as "abcd123"
 #When I click on login button
 #And I navigate to Leads page
 #Then I validate pagination of all leads page
 #
 #Scenario: Sign Up
 #When I navigate to Sign Up link
 #And I entered sign up details
 #
 #Scenario: Login to gmail and reply
 #Given I am on gmail login page
 #And I enter details to gmail page
 #
#
#
